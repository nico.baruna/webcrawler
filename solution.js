const request = require('request');
const requestPromise = require('request-promise');
const cheerio = require('cheerio');
const bluebird = require('bluebird');
const fs = require('fs');
const chalk = require('chalk');
const baseUrl = "https://www.bankmega.com/";
const outputFile = 'solution.json'
var result = {};


var options = {
    uri : "https://www.bankmega.com/promolainnya.php",
    transform : function (body){
        return cheerio.load(body);
    }
}
 
 //get category content
 function getAllPromo(url,pointer,result){
    
        if(pointer<url.length){
            var urlPage = [];
            var options = {
                uri : baseUrl+url[pointer],
                transform : function (body){
                    return cheerio.load(body);
                }
            }

            requestPromise(options)
                    .then(function($){

                        result[$('#subcatselected img').attr('title')] = [];
                        $('ul#promolain li').each(function(i)
                        {
                            result[$('#subcatselected img').attr('title')].push({
                                'title' : $(this).find('img').attr('title'),
                                'image' : baseUrl+$(this).find('img').attr('src'),
                                'url' : baseUrl+($(this).find('a').attr('href'))

                            });
                        });

                        var pageCount= $('.tablepaging tr').children().length;
                       
                        for(var i=0; i < pageCount-2;i++){
                            urlPage[i] = options.uri+"&page="+(i+1);
                          
                        } 
  
                    }).then(function(){
                        getAllPage(urlPage,1,result);
                    }).then(function(){
                        getAllPromo(url,pointer+1,result);
                    });
        }else{
            exportResults(result);
           return false;
        }  
}

//get all page content
function getAllPage(url,pointer,result){
    if(pointer<url.length){
        var options = {
            uri : url[pointer],
            transform : function (body){
                return cheerio.load(body);
            }
        }

        requestPromise(options)
                .then(function($){

                    $('ul#promolain li').each(function(i)
                    {
                        result[$('#subcatselected img').attr('title')].push({
                            'title' : $(this).find('img').attr('title'),
                            'image' : baseUrl+$(this).find('img').attr('src'),
                            'url' : baseUrl+($(this).find('a').attr('href'))

                        });
                    });

                }).then(function(){
                    getAllPage(url,pointer+1,result);
                });
    }else{
       //break the looop
       return false;
    }  
}


//export result
const exportResults = (result) => {
    fs.writeFile(outputFile, JSON.stringify(result, null, 4), (err) => {
      if (err) {
        console.log(err)
      }
      console.log(chalk.yellow.bgBlue(`\n ${chalk.underline.bold('Scrapping')} Results exported successfully to ${chalk.underline.bold(outputFile)}\n`))
    })
  }

console.log(chalk.yellow.bgBlue(`\n  Scraping of ${chalk.underline.bold(options.uri)} initiated...\n`));

//call first time
requestPromise(options)
              .then(function($){
                  //here to get subcat URL
                  var scriptJs= $('#contentpromolain2 script').html();
                  //mainCategories = scriptJs.match(/ajax.promolainnya.php\?product=[1-9]|[1-9]\d+$/g);
                  mainCategories = scriptJs.match(/ajax.promolainnya.php\?product=([0-9]|[1-9])\&subcat=([1-9]|[1-9])/g);
                  getAllPromo(mainCategories,0,result);
                  
              }).catch(function(err){
                  console.log(err);
              });

